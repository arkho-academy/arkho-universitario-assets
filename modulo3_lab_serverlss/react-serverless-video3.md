
# Pre-requisitos

1.  Instalación de [Node.js](https://nodejs.org/en/)
    
2.  Poseer editor de código, recomendado [Visual Studio Code](https://code.visualstudio.com/ "https://code.visualstudio.com/").
    

# Creación del proyecto

Como primer paso se necesita crear el proyecto React, para lo cual ejecutamos el siguiente comando:

```bash
npx create-react-app crud-serverless
```

El nombre de ejemplo para el proyecto será **crud-serverless**, pero puede crearlo con cualquier otro nombre que deseen. La creación del proyecto puede tomar algunos minutos.


Una vez creado, ejecutamos los siguientes comando para instalar algunas librerías necesarias:

```bash
cd crud-serverless npm install react-bootstrap bootstrap react-router-dom
```

# Iniciar el servidor y primero pasos

Para iniciar el servidor debe ser ejecutado desde la terminal del editor o uno externo:

```bash
 npm run start
 ```


# Creación de componentes

## Importar componente de estilos

Para obtener los estilos del la libreria [![](https://react-bootstrap.github.io/favicon.ico)React-Bootstrap](https://react-bootstrap.github.io/) se debe ajustar el archivo **index.js**, la alteración se puede observar en la linea 6:

```ts
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


```

## Componente del NavBar

El primer componente a ser creado es el **NavBar**, para esto en la carpeta **src** creamos el archivo **src/NavBar.js,** con el siguiente codigo:

```ts
import Container from 'react-bootstrap/Container';
import { Link } from "react-router-dom";
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';

function NavBar() {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">Serverless lab</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
          </Nav>
          <Nav className="justify-content-end">
          <Link to="/create">
            <Button variant="success" type="button">
              Create
            </Button>
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;
```

Este componente debe ser importado dentro del archivo **App.js** ubicado en ***scr/App.js***

```ts
import { BrowserRouter as Router  } from "react-router-dom";

import Navbar from './NavBar'
export default function App() {
  return (
    <Router>
      <div>
        <Navbar />
      </div>
    </Router>
  );
}
```

## Componente de listado

Este componente servirá para realizar el listado de los elementos almacenados en la tabla de DynamoDB:

El componente tendrá el nombre de **Tasks.js** en la ruta ***src/Tasks.js***

```ts
import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";

export default function Task() {
  const [tasks, setTasks] = useState([]);
  useEffect(() => {
    getTasks();
  }, []);

  //GET ALL USERS
  const getTasks = () => {
    fetch("https://xxxxx.execute-api.us-east-1.amazonaws.com/v1/test")
      .then((res) => res.json())
      .then((result) => {
        setTasks(result);
      });
  };

  const UpdateTask = (id) => {
    window.location = "/update/" + id;
  };

  const deleteTask = (id) => {
    fetch(
      `https://xxxxx.execute-api.us-east-1.amazonaws.com/v1/test/${id}`,
      {
        method: "DELETE",
        headers: {
          Accept: "application/form-data",
          "Content-Type": "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then((result) => {
        alert(result["message"]);
        window.location.href = "/";
      })
      .catch((result) => {
        alert("Error al eliminar");
        getTasks();
      });
  };

  return (
    <Card style={{ margin: "15px" }}>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {tasks.map((todo) => (
            <tr key={todo.id}>
              <td>{todo.id}</td>
              <td>{todo.title}</td>
              <td>{todo.description}</td>
              <td>
                <Button onClick={() => deleteTask(todo.id)} variant="danger">
                  Delete
                </Button>
                <Button onClick={() => UpdateTask(todo.id)} variant="primary">
                  Edit
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Card>
  );
}

```

Las lineas **14** y **27** deben ser reemplazados por el link obtenido en el API Gateway, con las funcionalidad de **Eliminar** ya implementado.


Importamos el componente dentro de **App.js**,

```ts
import { BrowserRouter as Router, Routes, Route  } from "react-router-dom";

import Navbar from './NavBar'
import Task from "./Tasks";

export default function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Routes>
          <Route exact path='/' element={<Task/>} />
        </Routes>
      </div>
    </Router>
  );
}
```


## Componente de creación

Para crear un nuevo registro creamos el componente **CreateTask.js** en la ruta ***src/CreateTask.js***

```ts
import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function CreateTask() {
  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      title: title,
      description: description,
    };
    console.log(data);
    fetch("https://xxxxxxx.execute-api.us-east-1.amazonaws.com/v1/test", {
      method: "POST",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert("Registrado con exito");
          window.location.href = "/";
      })
      .catch(() => {
        alert("Error al registrar");
          window.location.href = "/";
      });
  };

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  return (
    <Container className="d-flex h-100">
      <Row className="m-auto align-self-center">
        <Col>
          <Card>
            <Card.Header as="h5">Create</Card.Header>
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3" controlId="title">
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter title"
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="description">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter description"
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

```

Nuevamente reemplazar el link en la **linea 17** y editar el **App.js** para agregar la nueva funcionalidad:

```ts
import { BrowserRouter as Router, Routes, Route  } from "react-router-dom";

import Navbar from './NavBar'
import Task from "./Tasks";
import CreateTask from './CreateTask';

export default function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Routes>
          <Route exact path='/' element={<Task/>} />
          <Route exact path='/create' element={<CreateTask/>} />
        </Routes>
      </div>
    </Router>
  );
}
```


## Componente de edición:

Por ultimo se crea el archivo **UpdateTask.js** ***/scr/UpdateTask.js*** para actualizar los registros:
```ts
import React, { useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function UpdateTask() {
  const { id } = useParams();

  useEffect(() => {
    fetch(
      `https://xxxxxxx.execute-api.us-east-1.amazonaws.com/v1/test/${id}`
    )
      .then((res) => res.json())
      .then((result) => {
        setTitle(result.title);
        setDescription(result.description);
      });
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();
    var data = {
      id: id,
      title: title,
      description: description,
    };
    console.log(data);
    fetch("https://xxxxxx.execute-api.us-east-1.amazonaws.com/v1/test", {
      method: "PUT",
      headers: {
        Accept: "application/form-data",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => {
        alert("Registrado con exito");
        window.location.href = "/";
      })
      .catch(() => {
        alert("Error al registrar");
        window.location.href = "/";
      });
  };

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  return (
    <Container className="d-flex h-100">
      <Row className="m-auto align-self-center">
        <Col>
          <Card>
            <Card.Header as="h5">Edit</Card.Header>
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3" controlId="title">
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type="text"
                    value={title}
                    placeholder="Enter title"
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="description">
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    type="text"
                    value={description}
                    placeholder="Enter description"
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

```

E importar el componente en el **App.js** 

```ts
import { BrowserRouter as Router, Routes, Route  } from "react-router-dom";

import Navbar from './NavBar'
import Task from "./Tasks";
import CreateTask from './CreateTask';
import UpdateTask from "./UpdateTask";

export default function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Routes>
          <Route exact path='/' element={<Task/>} />
          <Route exact path='/create' element={<CreateTask/>} />
          <Route exact path='update/:id' element={<UpdateTask/>} />
        </Routes>
      </div>
    </Router>
  );
}
```

### Con esto queda finalizado el codigo